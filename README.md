# DX11 Engine

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/7f/Microsoft-DirectX-Logo-wordmark.svg/1200px-Microsoft-DirectX-Logo-wordmark.svg.png)](https://github.com/Microsoft/DirectXTK)

[![Build Status](https://travis-ci.org/AdamFull/DX11_engine.svg?branch=master)](https://travis-ci.org/AdamFull/DX11_engine)

# New Features!

  - Shader support
  - lua config support

### Ib dev
- Scene configurator
- Editor
